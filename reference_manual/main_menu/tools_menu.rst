.. meta::
   :description:
        The tools menu in Krita.

.. metadata-placeholder

   :authors: - Wolthera van Hövell tot Westerflier <griffinvalley@gmail.com>
             - Scott Petrovic
   :license: GNU free documentation license 1.3 or later.

.. index:: Macro, Scripts

.. _tools_menu:

==========
Tools Menu
==========

This contains three things.

Scripting
---------

When you have python scripting enabled and have scripts toggled, this is where most scripts are stored by default.

Recording
---------

.. caution::

    The recording and macro features are unmaintained and buggy.

Record a macro. You do this by pressing start, drawing something and then pressing stop. This feature can only record brush strokes. The resulting file is stored as a \*.kritarec file.

Macros
------

Play back or edit a krita rec file. The edit can only change the brush preset on strokes or add and remove filters.
