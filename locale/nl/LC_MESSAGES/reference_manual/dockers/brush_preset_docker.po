# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-03 13:55+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/brush_preset_docker.rst:1
msgid "Overview of the brush presets docker."
msgstr "Overzicht van de vastzetter Voorinstelling penseel."

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush Preset"
msgstr "Voorinstelling van penseel"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush"
msgstr "Penseel"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Presets"
msgstr "Voorinstellingen"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Paintop Presets"
msgstr "Voorinstellingen penseeleinde"

#: ../../reference_manual/dockers/brush_preset_docker.rst:16
msgid "Preset Docker"
msgstr "Voorinstelling vastzetter"

#: ../../reference_manual/dockers/brush_preset_docker.rst:19
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"

#: ../../reference_manual/dockers/brush_preset_docker.rst:20
msgid ""
"This docker allows you to switch the current brush you're using, as well as "
"tagging the brushes."
msgstr ""
"Deze vastzetter biedt u het omschakelen van het huidige in gebruik zijnde "
"penseel, evenals het geven van een tag aan de penselen."

#: ../../reference_manual/dockers/brush_preset_docker.rst:22
msgid "Just |mouseleft| on an icon to switch to that brush!"
msgstr "Doe |muislinks| op een pictogram om naar dat penseel om te schakelen!"

#: ../../reference_manual/dockers/brush_preset_docker.rst:25
msgid "Tagging"
msgstr "Tags toevoegen"

#: ../../reference_manual/dockers/brush_preset_docker.rst:27
msgid "|mouseright| a brush to add a tag or remove a tag."
msgstr "|muisrechts| een penseel om een tag toe te voegen of te verwijderen."
