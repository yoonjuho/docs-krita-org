# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-01 21:55+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:50
msgid ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"
msgstr ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: Perspektivrutnätsverktyg"

#: ../../reference_manual/tools/perspective_grid.rst:1
msgid "Krita's perspective grid tool reference."
msgstr "Referens för Kritas perspektivrutnätsverktyg."

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Perspective"
msgstr "Perspektiv"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Grid"
msgstr "Rutnät"

#: ../../reference_manual/tools/perspective_grid.rst:16
msgid "Perspective Grid Tool"
msgstr "Perspektivrutnätsverktyg"

#: ../../reference_manual/tools/perspective_grid.rst:18
msgid "|toolperspectivegrid|"
msgstr "|toolperspectivegrid|"

#: ../../reference_manual/tools/perspective_grid.rst:22
msgid "Deprecated in 3.0, use the :ref:`assistant_perspective` instead."
msgstr "Avråds från i 3.0, använd :ref:`assistant_perspective` istället."

#: ../../reference_manual/tools/perspective_grid.rst:24
msgid ""
"The perspective grid tool allows you to draw and manipulate grids on the "
"canvas that can serve as perspective guides for your painting. A grid can be "
"added to your canvas by first clicking the tool in the tool bar and then "
"clicking four points on the canvas which will serve as the four corners of "
"your grid."
msgstr ""
"Perspektivrutnätsverktyget låter dig rita och manipulera rutnät på duken som "
"kan fungera som perspektivlinjer för en målning. Ett rutnät kan läggas till "
"på duken genom att först klicka på verktyget i verktygsraden och därefter "
"klicka på fyra punkter på duken, vilket fungerar som i rutnätets fyra hörnen."

#: ../../reference_manual/tools/perspective_grid.rst:27
msgid ".. image:: images/tools/Perspectivegrid.png"
msgstr ".. image:: images/tools/Perspectivegrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:28
msgid ""
"The grid can be manipulated by pulling on any of its four corners. The grid "
"can be extended by clicking and dragging a midpoint of one of its edges. "
"This will allow you to expand the grid at other angles. This process can be "
"repeated on any subsequent grid or grid section. You can join the corners of "
"two grids by dragging one onto the other. Once they are joined they will "
"always move together, they cannot be separated. You can delete any grid by "
"clicking on the red X at its center. This tool can be used to build "
"reference for complex scenes."
msgstr ""
"Rutnätet kan manipuleras genom att dra i något av dess fyra hörn. Rutnätet "
"kan utökas genom att klicka och dra en mittpunkt på någon av dess kanter. "
"Det låter dig expandera rutnätet med andra vinklar. Processen kan upprepas "
"på vilket efterföljande rutnät eller rutnätssektion som helst. Det går att "
"sammanfoga hörnen på två rutnät genom att dra ett till det andra. När de väl "
"är sammanfogade flyttas de alltid tillsammans, och de kan inte separeras. "
"Det går att ta bort vilket rutnät som helst genom att klicka på det röda X:"
"et i mitten. Verktyget kan användas för att bygga en referens för komplexa "
"scener."

#: ../../reference_manual/tools/perspective_grid.rst:30
msgid "As displayed while the Perspective Grid tool is active: *"
msgstr "Som det visas när perspektivrutnätsverktyget är aktivt: *"

#: ../../reference_manual/tools/perspective_grid.rst:33
msgid ".. image:: images/tools/Multigrid.png"
msgstr ".. image:: images/tools/Multigrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:34
msgid "As displayed while any other tool is active: *"
msgstr "Som det visas när något annat verktyg är aktivt: *"

#: ../../reference_manual/tools/perspective_grid.rst:37
msgid ".. image:: images/tools/KritaPersgridnoedit.png"
msgstr ".. image:: images/tools/KritaPersgridnoedit.png"

#: ../../reference_manual/tools/perspective_grid.rst:38
msgid ""
"You can toggle the visibility of the grid from the main menu :menuselection:"
"`View --> Show Perspective Grid` option. You can also clear any grid setup "
"you have and start over by using the :menuselection:`View --> Clear "
"Perspective Grid`."
msgstr ""
"Rutnätets synlighet kan ändas från huvudmenyn med alternativet :"
"menuselection:`Visa --> Visa perspektivrutnät`. Det går också att rensa "
"eventuell rutnätsinställning som redan finns och börja om genom att använda :"
"menuselection:`Visa --> Rensa perspektivrutnät`."
