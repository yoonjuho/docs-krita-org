# Translation of docs_krita_org_general_concepts___file_formats___file_ora.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_ora\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_ora.rst:1
msgid "The Open Raster Archive file format as exported by Krita."
msgstr "Формат файлів Open Raster Archive у експортуванні за допомогою Krita."

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "*.ora"
msgstr "*.ora"

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "ORA"
msgstr "ORA"

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "Open Raster Archive"
msgstr "Open Raster Archive"

#: ../../general_concepts/file_formats/file_ora.rst:15
msgid "\\*.ora"
msgstr "\\*.ora"

#: ../../general_concepts/file_formats/file_ora.rst:17
msgid ""
"``.ora``, or the Open Raster format, is an interchange format. It was "
"designed to replace :ref:`file_psd` as an interchange format, as the latter "
"isn't meant for that. Like :ref:`file_kra` it is loosely based on the Open "
"Document structure, thus a ZIP file with a bunch of XMLs and PNGs, but where "
"Krita's internal file format can sometimes have fully binary chunks, ``."
"ora`` saves its layers as :ref:`file_png` making it fully open and easy to "
"support."
msgstr ""
"``.ora`` або Open Raster є форматом обміну даними між програмами. Його було "
"розроблено з метою замінити формат :ref:`file_psd`, оскільки останній від "
"початку не було призначено для вказаної мети. Подібно до :ref:`file_kra`, "
"цей формат засновано на структурі Open Document, тому є просто файл ZIP, у "
"якому міститься багато файлів XML та PNG, але там, де внутрішній формат "
"Krita може містити багато повних фрагментів двійкових даних, ``.ora`` "
"зберігає дані шарів у форматі :ref:`file_png`, що робить формат повністю "
"відкритим і простим для використання у інших програмах."

#: ../../general_concepts/file_formats/file_ora.rst:19
msgid ""
"As an interchange format, it can be expected to be heavy and isn't meant for "
"uploading to the internet."
msgstr ""
"Як формат для обміну даними, він містить забагато даних для звичайних "
"користувачів, тому його не призначено для вивантаження даних до інтернету."

#: ../../general_concepts/file_formats/file_ora.rst:23
msgid "`Open Raster Specification <https://www.openraster.org/>`_"
msgstr ""
"`Специфікація файлів відкритих растрових даних <https://www.openraster.org/"
">`_"
