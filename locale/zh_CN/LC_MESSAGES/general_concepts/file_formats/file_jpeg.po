msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_jpeg.pot\n"

#: ../../general_concepts/file_formats/file_jpeg.rst:1
msgid "The JPEG file format as exported by Krita."
msgstr "介绍由 Krita 导出的 JPEG 文件格式。"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpeg"
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpg"
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "*.jpg"
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:16
msgid "\\*.jpg"
msgstr "\\*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:18
msgid ""
"``.jpg``, ``.jpeg`` or ``.jpeg2000`` are a family of file-formats designed "
"to encode photographs."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:20
msgid ""
"Photographs have the problem that they have a lot of little gradients, which "
"means that you cannot index the file like you can with :ref:`file_gif` and "
"expect the result to look good. What JPEG instead does is that it converts "
"the file to a perceptual color space (:ref:`YCrCb <model_ycrcb>`), and then "
"compresses the channels that encode the colors, while keeping the channel "
"that holds information about the relative lightness uncompressed. This works "
"really well because human eye-sight is not as sensitive to colorfulness as "
"it is to relative lightness. JPEG also uses other :ref:`lossy "
"<lossy_compression>` compression techniques, like using cosine waves to "
"describe image contrasts."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:22
msgid ""
"However, it does mean that JPEG should be used in certain cases. For images "
"with a lot of gradients, like full scale paintings, JPEG performs better "
"than :ref:`file_png` and :ref:`file_gif`."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:24
msgid ""
"But for images with a lot of sharp contrasts, like text and comic book "
"styles, PNG is a much better choice despite a larger file size. For "
"grayscale images, :ref:`file_png` and :ref:`file_gif` will definitely be "
"more efficient."
msgstr ""

#: ../../general_concepts/file_formats/file_jpeg.rst:26
msgid ""
"Because JPEG uses lossy compression, it is not advised to save over the same "
"JPEG multiple times. The lossy compression will cause the file to reduce in "
"quality each time you save it. This is a fundamental problem with lossy "
"compression methods. Instead use a lossless file format, or a working file "
"format while you are working on the image."
msgstr ""
