msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___file_formats___file_ora.pot\n"

#: ../../general_concepts/file_formats/file_ora.rst:1
msgid "The Open Raster Archive file format as exported by Krita."
msgstr "Krita 导出的开放栅格图像归档文件格式。"

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "*.ora"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "ORA"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:10
msgid "Open Raster Archive"
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:15
msgid "\\*.ora"
msgstr "\\*.ora"

#: ../../general_concepts/file_formats/file_ora.rst:17
msgid ""
"``.ora``, or the Open Raster format, is an interchange format. It was "
"designed to replace :ref:`file_psd` as an interchange format, as the latter "
"isn't meant for that. Like :ref:`file_kra` it is loosely based on the Open "
"Document structure, thus a ZIP file with a bunch of XMLs and PNGs, but where "
"Krita's internal file format can sometimes have fully binary chunks, ``."
"ora`` saves its layers as :ref:`file_png` making it fully open and easy to "
"support."
msgstr ""

#: ../../general_concepts/file_formats/file_ora.rst:19
msgid ""
"As an interchange format, it can be expected to be heavy and isn't meant for "
"uploading to the internet."
msgstr "该文件格式用于数据交换，体积较大，不适合用来在互联网上分享作品。"

#: ../../general_concepts/file_formats/file_ora.rst:23
msgid "`Open Raster Specification <https://www.openraster.org/>`_"
msgstr "`开放栅格图像格式规范 <https://www.openraster.org/>`_"
