msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___gradient_draw.pot\n"

#: ../../<generated>:1
msgid "Antialias threshold"
msgstr ""

#: ../../<rst_epilog>:58
msgid ""
".. image:: images/icons/gradient_drawing_tool.svg\n"
"   :alt: toolgradient"
msgstr ""
".. image:: images/icons/gradient_drawing_tool.svg\n"
"   :alt: toolgradient"

#: ../../reference_manual/tools/gradient_draw.rst:1
msgid "Krita's gradient tool reference."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:11
msgid "Gradient"
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:16
msgid "Gradient Tool"
msgstr "渐变工具"

#: ../../reference_manual/tools/gradient_draw.rst:18
msgid "|toolgradient|"
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:20
msgid ""
"The Gradient tool is found in the Tools Panel. Left-Click dragging this tool "
"over the active portion of the canvas will draw out the current gradient.  "
"If there is an active selection then, similar to the :ref:`fill_tool`, the "
"paint action will be confined to the selection's borders."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:23
msgid "Tool Options"
msgstr "工具选项"

#: ../../reference_manual/tools/gradient_draw.rst:25
msgid "Shape:"
msgstr "形状："

#: ../../reference_manual/tools/gradient_draw.rst:27
msgid "Linear"
msgstr "线性"

#: ../../reference_manual/tools/gradient_draw.rst:28
msgid "This will draw the gradient straight."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:29
msgid "Radial"
msgstr "放射"

#: ../../reference_manual/tools/gradient_draw.rst:30
msgid ""
"This will draw the gradient from a center, defined by where you start the "
"stroke."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:31
msgid "Square"
msgstr "平方"

#: ../../reference_manual/tools/gradient_draw.rst:32
msgid ""
"This will draw the gradient from a center in a square shape, defined by "
"where you start the stroke."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:33
msgid "Conical"
msgstr "圆锥"

#: ../../reference_manual/tools/gradient_draw.rst:34
msgid ""
"This will wrap the gradient around a center, defined by where you start the "
"stroke."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:35
msgid "Conical-symmetric"
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:36
msgid ""
"This will wrap the gradient around a center, defined by where you start the "
"stroke, but will mirror the wrap once."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:38
msgid "Shaped"
msgstr "成形"

#: ../../reference_manual/tools/gradient_draw.rst:38
msgid "This will shape the gradient depending on the selection or layer."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:40
msgid "Repeat:"
msgstr "重复："

#: ../../reference_manual/tools/gradient_draw.rst:42
msgid "None"
msgstr "未分组"

#: ../../reference_manual/tools/gradient_draw.rst:43
msgid "This will extend the gradient into infinity."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:44
msgid "Forward"
msgstr "转发"

#: ../../reference_manual/tools/gradient_draw.rst:45
msgid "This will repeat the gradient into one direction."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:47
msgid "Alternating"
msgstr "交替"

#: ../../reference_manual/tools/gradient_draw.rst:47
msgid ""
"This will repeat the gradient, alternating the normal direction and the "
"reversed."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:49
msgid "Reverse"
msgstr "逆序"

#: ../../reference_manual/tools/gradient_draw.rst:50
msgid "Reverses the direction of the gradient."
msgstr ""

#: ../../reference_manual/tools/gradient_draw.rst:52
msgid "Doesn't do anything, original function must have gotten lost in a port."
msgstr ""
