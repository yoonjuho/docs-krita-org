# Translation of docs_krita_org_reference_manual___dockers___brush_preset_docker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:39+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: clic dret del ratolí"

#: ../../reference_manual/dockers/brush_preset_docker.rst:1
msgid "Overview of the brush presets docker."
msgstr "Vista general de l'acoblador Pinzells predefinits."

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush Preset"
msgstr "Pinzell predefinit"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Brush"
msgstr "Pinzell"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Presets"
msgstr "Predefinits"

#: ../../reference_manual/dockers/brush_preset_docker.rst:11
msgid "Paintop Presets"
msgstr "Predefinits de «paintop»"

#: ../../reference_manual/dockers/brush_preset_docker.rst:16
msgid "Preset Docker"
msgstr "Acoblador Pinzells predefinits"

#: ../../reference_manual/dockers/brush_preset_docker.rst:19
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"

#: ../../reference_manual/dockers/brush_preset_docker.rst:20
msgid ""
"This docker allows you to switch the current brush you're using, as well as "
"tagging the brushes."
msgstr ""
"Aquest acoblador permet canviar el pinzell actual que esteu utilitzant, així "
"com etiquetar-los."

#: ../../reference_manual/dockers/brush_preset_docker.rst:22
msgid "Just |mouseleft| on an icon to switch to that brush!"
msgstr ""
"Simplement feu |mouseleft| sobre una icona per canviar a aquest pinzell."

#: ../../reference_manual/dockers/brush_preset_docker.rst:25
msgid "Tagging"
msgstr "Etiquetar"

#: ../../reference_manual/dockers/brush_preset_docker.rst:27
msgid "|mouseright| a brush to add a tag or remove a tag."
msgstr "Feu |mouseright| sobre un pinzell per afegir o eliminar una etiqueta."
