# translation of docs_krita_org_reference_manual___brushes___brush_settings___locked_brush_settings.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_settings___locked_brush_settings\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 08:54+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Unlock (Keep Locked)"
msgstr ""

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:1
msgid "How to keep brush settings locked in Krita."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:11
#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:16
msgid "Locked Brush Settings"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:19
msgid ""
"Normally, a changing to a different brush preset will change all brush "
"settings. Locked presets are a way for you to prevent Krita from changing "
"all settings. So, if you want to have the texture be that same over all "
"brushes, you lock the texture parameter. That way, all brush-preset you "
"select will now share the same texture!"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:22
msgid "Locking a brush parameter"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:25
#, fuzzy
#| msgid ".. image:: images/en/Krita_2_9_brushengine_locking_01.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_locking_01.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:26
msgid ""
"To lock an option, |mouseright| the little lock icon next to the parameter "
"name, and set it to :guilabel:`Lock`. It will now be highlighted to show "
"it's locked:"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:29
#, fuzzy
#| msgid ".. image:: images/en/Krita_2_9_brushengine_locking_02.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_locking_02.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:30
msgid "And on the canvas, it will show that the texture-option is locked."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:33
#, fuzzy
#| msgid ".. image:: images/en/Krita_2_9_brushengine_locking_04.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_locking_04.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:35
msgid "Unlocking a brush parameter"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:37
msgid "To *unlock*, |mouseright| the icon again."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:40
#, fuzzy
#| msgid ".. image:: images/en/Krita_2_9_brushengine_locking_03.png"
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_locking_03.png"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:41
msgid "There will be two options:"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:43
msgid "Unlock (Drop Locked)"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:44
msgid ""
"This will get rid of the settings of the locked parameter and take that of "
"the active brush preset. So if your brush had no texture on, using this "
"option will revert it to having no texture."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:46
msgid "This will keep the settings of the parameter even though it's unlocked."
msgstr ""
